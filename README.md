# Example

## Instanciate a parser service

```php
// Parser manager
// 'my_parser.manager' service is a custom class extending \Sgostanyan\Sgt_parser\Parser\ParserBase
$parserManager = Drupal::service('my_parser.manager');
```

## CSV

```php
    // File uri.
    $uri = '/list.csv';

    // Initialise parser
    $parserManager->setParserType(new CsvParser());

    // Read file.
    $parsedCsvArray = $parserManager->read($uri);
```

## XML

```php
    // File uri.
    $uri = '/list.xml';

    // Initialise parser
    $parserManager->setParserType(new XmlParser());

    // Read file.
    $parsedXmlArray = $parserManager->read($uri);
```

## RSS

```php
    // URL.
    $url = 'https://madeinfoot.ouest-france.fr/flux/rss_ligue.php?id=16';

    // Initialise parser
    $parserManager->setParserType(new RssParser());

    // Read file.
    $parsedRssArray = $parserManager->read($url);
```

You can create you own parser type by implementing ParserInterface.