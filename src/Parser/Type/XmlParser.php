<?php

namespace Sgostanyan\Sgt_parser\Parser\Type;

/**
 * Class XmlParser
 *
 * @package Sgostanyan\Sgt_parser\Parser\Type
 */
class XmlParser implements ParserInterface {

  /**
   * @param $filePath
   * @param $options
   *
   * @return mixed
   */
  public function parse($filePath, $options = []) {

    $xml = simplexml_load_string(file_get_contents($filePath), "SimpleXMLElement", LIBXML_NOCDATA);
    $json = json_encode($xml);
    return json_decode($json, TRUE);

  }

  public function write() {
    // TODO: Implement create() method.
  }
}
