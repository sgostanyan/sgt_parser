<?php

namespace Sgostanyan\Sgt_parser\Parser\Type;

/**
 * Interface ParserInterface
 *
 * @package Sgostanyan\Sgt_parser\Parser\Type
 */
interface ParserInterface {

  /**
   * @param string $filePath
   * @param array $options
   *
   * @return mixed
   */
  public function parse(string $filePath, array $options = []);

  /**
   * @return mixed
   */
  public function write();

}
