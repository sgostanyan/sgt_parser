<?php

namespace Sgostanyan\Sgt_parser\Parser\Type;

/**
 * Class CsvParser
 *
 * @package Sgostanyan\Sgt_parser\Parser\Type
 */
class CsvParser implements ParserInterface {

  /**
   * @param $filePath
   * @param $options
   * array with key value options (separator, enclosure, escape, length)
   *
   * @return array|false
   */
  public function parse($filePath, $options = []) {

    $separator = !empty($options['separator']) ? $options['separator'] : ',';
    $enclosure = !empty($options['enclosure']) ? $options['enclosure'] : '"';
    $escape = !empty($options['escape']) ? $options['escape'] : '\\';
    $length = !empty($options['length']) ? $options['length'] : 0;

    $stream = fopen($filePath, 'r');
    if ($stream) {
      $rows = [];
      while ($row = fgetcsv($stream, $length, $separator, $enclosure, $escape)) {
        $rows[] = $row;
      }
      fclose($stream);
      return $rows;
    }
    return FALSE;

  }

  public function write() {
    // TODO: Implement create() method.
  }
}
