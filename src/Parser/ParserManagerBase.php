<?php

namespace Sgostanyan\Sgt_parser\Parser;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Sgostanyan\Sgt_parser\Parser\Type\ParserInterface;

/**
 * Class ParserManagerBase
 *
 * @package Sgostanyan\Sgt_parser\Parser
 */
abstract class ParserManagerBase {

  /**
   * @var \Sgostanyan\Sgt_parser\Parser\Type\ParserInterface
   */
  protected ParserInterface $parser;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapper;

  /**
   * Parser constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapper
   */
  public function __construct(FileSystemInterface $fileSystem, StreamWrapperManagerInterface $streamWrapper) {
    $this->fileSystem = $fileSystem;
    $this->streamWrapper = $streamWrapper;
  }

  /**
   * @param \Sgostanyan\Sgt_parser\Parser\Type\ParserInterface $parser
   *
   * @return void
   */
  public function setParserType(ParserInterface $parser) {
    $this->parser = $parser;
  }

  /**
   * @param $uri
   *
   * @return mixed
   * @throws \Exception
   */
  public function read($uri) {

    if ($this->streamWrapper->isValidUri($uri)) {
      $realPath = $this->fileSystem->realpath($uri);
      return $this->parser->parse($realPath);
    }
    else {
      try {
        return $this->parser->parse($uri);
      }
      catch (\Exception $e) {
        throw new \Exception($e->getMessage());
      }
    }
  }

}
